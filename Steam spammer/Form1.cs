﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SteamKit2;
using System.Threading;
using System.IO;
using System.Security.Cryptography;
using Microsoft.VisualBasic;

/*  Steam spammer made by DevPhreak
 *  
 *  A lot of this code is Copy Pasted, I'll keep building upon it
 * 
 * Thanks to https://github.com/SteamRE/SteamKit for the library and samples.
 */

namespace Steam_spammer
{
    public partial class Form1 : Form
    {

        SteamClient steamClient;
        CallbackManager manager;
        SteamUser steamUser;
        SteamFriends steamFriends;

        bool isRunning, loggedoff, allowsending;

        string user, pass;
        string authCode, twoFactorAuth;
        Dictionary<string, SteamID> friends = new Dictionary<string, SteamID>();

        public Form1()
        {
            InitializeComponent();
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.sendAmount, "Set the value to 0 for infinite.");
            ToolTip1.IsBalloon = true;

            System.Windows.Forms.ToolTip ToolTip2 = new System.Windows.Forms.ToolTip();
            ToolTip2.SetToolTip(this.checkOnline, "This also works for refreshing purposes.");
            ToolTip2.IsBalloon = true;
        }

        private void Loginbtn_Click(object sender, EventArgs e)
        {
            if (!isRunning)
            {
                if (username.Text.Length > 2 && password.Text.Length > 6)
                {
                    user = username.Text;
                    pass = password.Text;

                    steamClient = new SteamClient();
                    manager = new CallbackManager(steamClient);
                    steamUser = steamClient.GetHandler<SteamUser>();

                    Thread th = new Thread(callbackmanager);
                    th.IsBackground = true;

                    steamUser = steamClient.GetHandler<SteamUser>();
                    steamFriends = steamClient.GetHandler<SteamFriends>();

                    manager.Subscribe<SteamClient.ConnectedCallback>(OnConnected);
                    manager.Subscribe<SteamClient.DisconnectedCallback>(OnDisconnected);
                    manager.Subscribe<SteamUser.LoggedOnCallback>(OnLoggedOn);
                    manager.Subscribe<SteamUser.LoggedOffCallback>(OnLoggedOff);
                    manager.Subscribe<SteamUser.AccountInfoCallback>(OnAccountInfo);
                    manager.Subscribe<SteamFriends.FriendsListCallback>(OnFriendsList);
                    manager.Subscribe<SteamUser.UpdateMachineAuthCallback>(OnMachineAuth);

                    isRunning = true;
                    loggedoff = false;
                    steamClient.Connect();
                    th.Start();
                }
                else
                {
                    MessageBox.Show("Please fill in your username and password");
                }
            }
            else
            {
                MessageBox.Show("Already logged in!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (isRunning)
            {
                steamUser.LogOff();
                isRunning = false;
                loginfo("Logged off");
                loggedoff = true;
                friends.Clear();
            }
            else
            {
                loginfo("You need to be logged in to log out.");
            }
        }

        public void callbackmanager()
        {
            while (isRunning)
            {
                manager.RunWaitCallbacks(TimeSpan.FromSeconds(1));
            }
        }

        public void loginfo(string text)
        {
            this.Invoke((MethodInvoker)delegate
            {
                logbox.AppendText(text + Environment.NewLine);
            });
        }

        public void sendmessage()
        {
            this.Invoke((MethodInvoker)delegate
            {
                steamFriends.SendChatMessage((SteamID)friendlist.SelectedValue, EChatEntryType.ChatMsg, spamText.Text);
            });
        }

        private void sendBtn_Click(object sender, EventArgs e)
        {
            if (isRunning)
            {
                if (spamText.Text.Length > 0 && friendlist.SelectedValue != null)
                {
                    int iteration = 0;
                    if (sendAmount.Value > 0)
                    {
                        allowsending = true;
                        new Thread(() =>
                        {
                            Thread.CurrentThread.IsBackground = true;
                            while (iteration < sendAmount.Value)
                            {
                                if (allowsending)
                                {
                                    sendmessage();
                                    iteration++;
                                    Thread.Sleep(TimeSpan.FromSeconds(0.1));
                                }
                            }
                            loginfo(String.Format("Succesfully sent {0} messages.", iteration));
                            allowsending = false;
                        }).Start();
                    }
                    else
                    {
                        allowsending = true;
                        new Thread(() =>
                     {
                         Thread.CurrentThread.IsBackground = true;
                         do
                         {
                             sendmessage();
                             Thread.Sleep(TimeSpan.FromSeconds(0.1));
                             iteration++;
                         }
                         while (allowsending);
                         loginfo(String.Format("Succesfully sent {0} messages.", iteration));
                         allowsending = false;
                     }).Start();
                    }

                }
                else
                {
                    MessageBox.Show("Please select the target and enter a text");
                }
            }
            else
            {
                MessageBox.Show("I would suggest logging in first.");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (allowsending)
            {
                allowsending = false;
            }
        }

        public void binddata()
        {
            this.Invoke((MethodInvoker)delegate
            {
                if (checkOnline.Checked)
                {
                    Dictionary<string, SteamID> onlinefriends = new Dictionary<string, SteamID>();
                    foreach (var friend in friends)
                    {
                        if (steamFriends.GetFriendPersonaState(friend.Value) != EPersonaState.Offline)
                        {
                            onlinefriends.Add(friend.Key, friend.Value);
                        }
                    }
                    friendlist.DataSource = new BindingSource(onlinefriends, null);
                    friendlist.ValueMember = "Value";
                    friendlist.DisplayMember = "Key";
                }
                else
                {
                    friendlist.DataSource = new BindingSource(friends, null);
                    friendlist.ValueMember = "Value";
                    friendlist.DisplayMember = "Key";
                }
            });
        }

        private void checkOnline_CheckedChanged(object sender, EventArgs e)
        {
            if (isRunning)
            {
                binddata();
            }
        }

        void OnConnected(SteamClient.ConnectedCallback callback)
        {
            if (callback.Result != EResult.OK)
            {

                loginfo(String.Format("Unable to connect to Steam: {0}.", callback.Result));
                isRunning = false;
                return;
            }
            loginfo(String.Format("Connected to Steam! Attempting to log {0} in", user));

            byte[] sentryHash = null;
            if (File.Exists("sentry.bin"))
            {
                byte[] sentryFile = File.ReadAllBytes("sentry.bin");
                sentryHash = CryptoHelper.SHAHash(sentryFile);
            }

            isRunning = true;
            steamUser.LogOn(new SteamUser.LogOnDetails
            {
                Username = user,
                Password = pass,
                AuthCode = authCode,
                TwoFactorCode = twoFactorAuth,
                SentryFileHash = sentryHash,
            });
        }

        void OnDisconnected(SteamClient.DisconnectedCallback callback)
        {
            if (!loggedoff)
            {
                loginfo("Disconnected from Steam, reconnecting in 3 seconds.");
                Thread.Sleep(TimeSpan.FromSeconds(3));
                steamClient.Connect();
            }
            else
            {
                loginfo("Disconnected from Steam.");
            }
        }

        void OnLoggedOn(SteamUser.LoggedOnCallback callback)
        {
            bool isSteamGuard = callback.Result == EResult.AccountLogonDenied;
            bool is2FA = callback.Result == EResult.AccountLoginDeniedNeedTwoFactor;

            if (isSteamGuard || is2FA)
            {
                loginfo("This account is SteamGuard protected!");

                if (is2FA)
                {
                    loginfo("2FA detected.");
                    twoFactorAuth = Interaction.InputBox("Please enter your 2FA code so we can login", "2FA code.", "2FA code goes here", -1, -1).Trim();
                }
                else
                {
                    loginfo("Auth detected");
                    authCode = Interaction.InputBox("Please enter your auth code sent to your email", "Auth code", "Auth code goes here", -1, -1).Trim();
                }
                return;
            }

            if (callback.Result != EResult.OK)
            {
                loginfo(String.Format("Unable to logon to Steam: {0} / {1}", callback.Result, callback.ExtendedResult));
                isRunning = false;
                return;
            }

            loginfo("Successfully logged on!");
        }

        void OnLoggedOff(SteamUser.LoggedOffCallback callback)
        {
            loginfo(String.Format("Logged off of Steam: {0}", callback.Result));
        }

        void OnAccountInfo(SteamUser.AccountInfoCallback callback)
        {
            steamFriends.SetPersonaState(EPersonaState.Online);
        }

        void OnFriendsList(SteamFriends.FriendsListCallback callback)
        {
            int friendCount = steamFriends.GetFriendCount();
            loginfo(String.Format("We currently have {0} friends", friendCount));
            Thread.Sleep(TimeSpan.FromSeconds(1));
            for (int x = 0; x < friendCount; x++)
            {
                SteamID steamIdFriend = steamFriends.GetFriendByIndex(x);
                loginfo(String.Format("Friend {0}: {1}", x, steamFriends.GetFriendPersonaName(steamIdFriend)));
                friends.Add(steamFriends.GetFriendPersonaName(steamIdFriend), steamIdFriend);
            }
            binddata();
        }

        void OnMachineAuth(SteamUser.UpdateMachineAuthCallback callback)
        {
            loginfo("Updating sentryfile...");
            int fileSize;
            byte[] sentryHash;
            using (var fs = File.Open("sentry.bin", FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                fs.Seek(callback.Offset, SeekOrigin.Begin);
                fs.Write(callback.Data, 0, callback.BytesToWrite);
                fileSize = (int)fs.Length;

                fs.Seek(0, SeekOrigin.Begin);
                using (var sha = new SHA1CryptoServiceProvider())
                {
                    sentryHash = sha.ComputeHash(fs);
                }
            }
            steamUser.SendMachineAuthResponse(new SteamUser.MachineAuthDetails
            {
                JobID = callback.JobID,

                FileName = callback.FileName,

                BytesWritten = callback.BytesToWrite,
                FileSize = fileSize,
                Offset = callback.Offset,

                Result = EResult.OK,
                LastError = 0,

                OneTimePassword = callback.OneTimePassword,

                SentryFileHash = sentryHash,
            });

            loginfo("Done!");
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);    //somehow threads didnt close, even though theyre set to background...
        }
    }
}