﻿namespace Steam_spammer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkOnline = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.sendAmount = new System.Windows.Forms.NumericUpDown();
            this.sendBtn = new System.Windows.Forms.Button();
            this.friendlist = new System.Windows.Forms.ComboBox();
            this.spamText = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.Loginbtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.TextBox();
            this.username = new System.Windows.Forms.TextBox();
            this.logbox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sendAmount)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkOnline);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.sendAmount);
            this.groupBox1.Controls.Add(this.sendBtn);
            this.groupBox1.Controls.Add(this.friendlist);
            this.groupBox1.Controls.Add(this.spamText);
            this.groupBox1.Location = new System.Drawing.Point(220, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(307, 111);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Setup";
            // 
            // checkOnline
            // 
            this.checkOnline.AutoSize = true;
            this.checkOnline.Location = new System.Drawing.Point(197, 20);
            this.checkOnline.Name = "checkOnline";
            this.checkOnline.Size = new System.Drawing.Size(56, 17);
            this.checkOnline.TabIndex = 6;
            this.checkOnline.Text = "Online";
            this.checkOnline.UseVisualStyleBackColor = true;
            this.checkOnline.CheckedChanged += new System.EventHandler(this.checkOnline_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(245, 76);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(59, 20);
            this.button2.TabIndex = 5;
            this.button2.Text = "Stop";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // sendAmount
            // 
            this.sendAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sendAmount.Location = new System.Drawing.Point(268, 19);
            this.sendAmount.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.sendAmount.Name = "sendAmount";
            this.sendAmount.Size = new System.Drawing.Size(33, 20);
            this.sendAmount.TabIndex = 3;
            // 
            // sendBtn
            // 
            this.sendBtn.Location = new System.Drawing.Point(245, 50);
            this.sendBtn.Name = "sendBtn";
            this.sendBtn.Size = new System.Drawing.Size(59, 20);
            this.sendBtn.TabIndex = 4;
            this.sendBtn.Text = "Send";
            this.sendBtn.UseVisualStyleBackColor = true;
            this.sendBtn.Click += new System.EventHandler(this.sendBtn_Click);
            // 
            // friendlist
            // 
            this.friendlist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.friendlist.FormattingEnabled = true;
            this.friendlist.Location = new System.Drawing.Point(6, 19);
            this.friendlist.Name = "friendlist";
            this.friendlist.Size = new System.Drawing.Size(185, 21);
            this.friendlist.Sorted = true;
            this.friendlist.TabIndex = 2;
            // 
            // spamText
            // 
            this.spamText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spamText.Location = new System.Drawing.Point(6, 49);
            this.spamText.Multiline = true;
            this.spamText.Name = "spamText";
            this.spamText.Size = new System.Drawing.Size(233, 48);
            this.spamText.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.Loginbtn);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.password);
            this.groupBox2.Controls.Add(this.username);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 111);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Login";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(119, 76);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Logout";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Loginbtn
            // 
            this.Loginbtn.Location = new System.Drawing.Point(9, 76);
            this.Loginbtn.Name = "Loginbtn";
            this.Loginbtn.Size = new System.Drawing.Size(75, 23);
            this.Loginbtn.TabIndex = 2;
            this.Loginbtn.Text = "Login";
            this.Loginbtn.UseVisualStyleBackColor = true;
            this.Loginbtn.Click += new System.EventHandler(this.Loginbtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Username";
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(67, 50);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(100, 20);
            this.password.TabIndex = 1;
            this.password.UseSystemPasswordChar = true;
            // 
            // username
            // 
            this.username.Location = new System.Drawing.Point(67, 19);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(100, 20);
            this.username.TabIndex = 0;
            // 
            // logbox
            // 
            this.logbox.BackColor = System.Drawing.SystemColors.ControlLight;
            this.logbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.logbox.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.logbox.Location = new System.Drawing.Point(12, 129);
            this.logbox.Multiline = true;
            this.logbox.Name = "logbox";
            this.logbox.ReadOnly = true;
            this.logbox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logbox.Size = new System.Drawing.Size(515, 79);
            this.logbox.TabIndex = 2;
            this.logbox.UseWaitCursor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 220);
            this.Controls.Add(this.logbox);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Steam spammer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sendAmount)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Loginbtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox logbox;
        private System.Windows.Forms.ComboBox friendlist;
        private System.Windows.Forms.TextBox spamText;
        private System.Windows.Forms.Button sendBtn;
        private System.Windows.Forms.NumericUpDown sendAmount;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox checkOnline;
    }
}

